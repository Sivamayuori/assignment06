//This is a program to print a number triangle for n
#include<stdio.h>
//Function for the number of lines of the pattern
void pattern (int n);

void pattern (int n){
	int i, j;

	if (n==0)
		return pattern(n);

	else{
		for (i=1; i<=n; i++){
			for (j=i; j>=1; j--){
				printf ("%d", j);
			}
			printf ("\n");
		}
	}
}

int main(){
	int n;
	//Display the entered number
	printf ("Please enter the number: ");
	//Reading the entered value
	scanf ("%d", &n);
	pattern(n);
	//Display the pattern
	printf ("\n");

	return 0;
}

