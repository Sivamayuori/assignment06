//This is a program to print the fibonacci sequence upto n
#include<stdio.h>
//Function for the fibonacci sequence
int fibonacci(int);

int main(void){
	int x;

	//Display the entered number of temrs
	printf("Enter the number of terms: ");
	//Reading the entered value
	scanf("%d", &x);
	//Display the terms of fibonacci sequence 
	printf("First %d terms of Fibonacci series are: \n", x);

	for(int n=0; n<x; n++){
		printf("%d \n", fibonacci(n));
	}

	return 0;
}

int fibonacci(int num){
	if(num==0||num==1){
		return num;
	}
	else{
		return fibonacci(num-1)+fibonacci(num-2);
	}
}

